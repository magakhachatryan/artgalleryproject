﻿using ArtGalleryDTO.EntitiesDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryBLL.InterfacesBLL
{
    public interface IShoppingCart
    {
        IUserService userService { get; set; }
        IArtObjectService artObjectService { get; set; }

        Task<ShoppingCartItemDTO> BuyObject(ShoppingCartItemDTO model);
        
    }
}
