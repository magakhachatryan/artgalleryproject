﻿using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryBLL.ServicesBLL
{
    public class ShoppingCartService : ServiceBase<ShoppingCartItem, ShoppingCartItemDTO>, IShoppingCart
    {


        public ShoppingCartService(IMapper mapper, ArtGalleryContext context) : base(mapper, context)
        {

        }

        public IUserService userService { get; set; }
        public IArtObjectService artObjectService { get; set; }

        public async Task<ShoppingCartItemDTO> BuyObject(ShoppingCartItemDTO model)
        {
            //var data = mapper.Map<ShoppingCartItem>(model);
            ///poxy poxancec
            var obj = await artObjectService.GetById(model.artObjectId);
            obj.UserId = model.UserId;
            await artObjectService.Update(obj);
            var result = await Remove(model.Id);
            return result;
        }

    }
}
