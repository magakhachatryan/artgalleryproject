﻿using System;
using System.Collections.Generic;
using System.Text;
using ArtGalleryDTO.EntitiesDTO;
using ArtGalleryDAL.Entities;
using ArtGalleryBLL.InterfacesBLL;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using ArtGalleryDAL;
namespace ArtGalleryBLL.ServicesBLL
{
    public class ArtObjectService :ServiceBase<ArtObject, ArtObjectDTO>, IArtObjectService
    {
        public ArtObjectService(IMapper  mapper, ArtGalleryContext context) : base(mapper,context)
        {

        }

        
    }
}
