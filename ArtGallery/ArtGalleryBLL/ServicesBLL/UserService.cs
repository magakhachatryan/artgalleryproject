﻿using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using AutoMapper;
using ArtGalleryDAL;
using System;
using ArtGalleryDAL.Entities;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryBLL.ServicesBLL
{
    public class UserService : ServiceBase<User, UserDTO>, IUserService
    {
        public UserService(IMapper mapper,ArtGalleryContext context):base(mapper,context)
        {
            
        }

    }
}
