﻿using System;
using System.Collections.Generic;
using ArtGalleryDTO.InterfacesDTO;
using Enums;

namespace ArtGalleryDTO.EntitiesDTO
{
    public class ArtObjectDTO :IBaseEntityDTO
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string artist { get; set; }
        public Types type { get; set; }
        public string description { get; set; }
        public DateTime time { get; set; }
        public int UserId { get; set; }
        public decimal price { get; set; }

        public ICollection<ShoppingCartItemDTO> Carts { get; set; }
    }
}
