﻿using ArtGalleryDTO.InterfacesDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDTO.EntitiesDTO
{
    public class ShoppingCartItemDTO : IBaseEntityDTO
    {
        public int Id { get; set; }
        public int artObjectId { get; set; }
        public int UserId { get; set; }

    }
}