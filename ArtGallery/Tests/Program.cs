﻿using System;
using AutoMapperLib;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using Enums;
using System.Collections.Generic;
using AutoMapper;
using ArtGalleryDAL;

namespace Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            UserDTO user = new UserDTO();
            user.Id = 1;
            user.Name = "Maga";
            user.Address = "123456";
            user.Account = "qajaran";
            ArtObjectDTO obj = new ArtObjectDTO();
            obj.name = "painting";
            obj.Id = 1;
            obj.description = "dghsnkjx";
            obj.artist = "es";
            obj.type = Types.Painting;
            obj.UserId = 1;
            List<ArtObjectDTO> list = new List<ArtObjectDTO>();
            list.Add(obj);
            user.objects = list;
            var config = new MapperConfiguration(cfg => {

                cfg.CreateMap<User, UserDTO>()
                .ReverseMap();
                cfg.CreateMap<ArtObject, ArtObjectDTO>()
                 .ForMember(s => s.UserId, opt => opt.MapFrom(src => src.user.Id))
                .ReverseMap()
                .ForPath(s => s.user, opt => opt.Ignore()); ;
            });
            //config.AssertConfigurationIsValid();

            var mapper = config.CreateMapper();
            var dest = mapper.Map<ArtObjectDTO, ArtObject>(obj);
            Console.ReadLine();
           
        }
    }
}
