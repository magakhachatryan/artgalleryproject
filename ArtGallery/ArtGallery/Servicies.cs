﻿using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryBLL.ServicesBLL;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArtGallery
{
    static public class Servicies
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<ISellerService, SellerService>();
            services.AddScoped<IArtObjectService, ArtObjectService>();
            services.AddScoped<IShoppingCart, ShoppingCartService>();
            return services;
        }
    }
}
