﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ArtGallery.Controllers
{
    [Route("api/shoppingcart")]
    [ApiController]
    public class ShoppingCartController : ControllerBase
    {
        private IShoppingCart shoppingCart;

        public ShoppingCartController(IShoppingCart shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }
        // GET: api/ShoppingCartItem
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ShoppingCartItem/5
        [HttpGet("{id}")]//, Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ShoppingCartItem
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/ShoppingCartItem/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Buy([FromBody] ShoppingCartDTO model)
        {
        }
    }
}
