﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ArtGallery.Controllers
{
    [Route("api/seller")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        private ISellerService sellerService;
        public SellerController(ISellerService sellerService)
        {
            this.sellerService = sellerService;
        }
        // GET: api/Seller
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var data = await sellerService.GetAll();
                return Ok(data);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/Seller/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var data = await sellerService.GetById(id);
                return Ok(data.Id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/Seller
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] SellerDTO value)
        {

            try
            {
                var data = await sellerService.Add(value);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT: api/Seller/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] SellerDTO value)
        {
            if (id == value.Id)
            {
                 var data=await sellerService.Update(value);
                return Ok(data.Id);
            }
            return BadRequest(id);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var data = await sellerService.GetById(id);
            if (data == null)
            {
                return BadRequest(id);
            }
            else
            {
                //var item=userService.Remove(id);
                return Ok(sellerService.Remove(id).Id);
            }
        }
    }
}
