﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;


namespace AutoMapperLib
{
    public static class AutoMapper
    {
        public static IServiceCollection ConfigureAutomapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MapProfile));
            return services;
        }
    }
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<User, UserDTO>()
            .ReverseMap();
            CreateMap<ArtObject, ArtObjectDTO>()
              .ForMember(s => s.UserId, opt => opt.MapFrom(src => src.user.Id))
              .ReverseMap()
              .ForPath(s => s.user, opt => opt.Ignore());

            CreateMap<ShoppingCartItem, ShoppingCartItemDTO>()
             .ForMember(s => s.UserId, opt => opt.MapFrom(src => src.user.Id))
             .ForMember(s => s.artObjectId, opt => opt.MapFrom(src => src.artObject.Id))
             .ReverseMap()
             .ForPath(s => s.user, opt => opt.Ignore())
             .ForPath(s => s.artObject, opt => opt.Ignore());

        }
    }
}

