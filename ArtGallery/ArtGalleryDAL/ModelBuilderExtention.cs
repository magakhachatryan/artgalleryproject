﻿using System;
using System.Collections.Generic;
using System.Text;
using ArtGalleryDAL.Entities;

using Microsoft.EntityFrameworkCore;

namespace ArtGalleryDAL
{
    

        public static class ModelBuilderExtention
        {
            public static void SeedData(this ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<User>().HasData(
                new User { Id = 1, Name = "User1" ,Address="hbhj",Account="123"},
                new User { Id = 2, Name = "User2", Address = "hbhj", Account = "123" });


            modelBuilder.Entity<ArtObject>().HasData(
                new ArtObject { Id= 1, name="Shout", artist="Munk"},
                new ArtObject { Id = 2, name = "Shout", artist = "Munk" }
                );
           
            }
        }
    
}
