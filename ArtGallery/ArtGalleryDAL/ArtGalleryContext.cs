﻿using Microsoft.EntityFrameworkCore;
using ArtGalleryDAL.Entities;
namespace ArtGalleryDAL
{
    public class ArtGalleryContext : DbContext
    {

        public ArtGalleryContext(DbContextOptions<ArtGalleryContext> options) : base(options)
        {

            Database.Migrate();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<ArtObject> Objects { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<User>()
                .HasKey(p => p.Id);
            modelBuilder.Entity<User>()
                .Property(f => f.Id)
                .ValueGeneratedOnAdd();

             
            modelBuilder.Entity<ArtObject>()
                .Property(f => f.Id)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<ArtObject>()
                .HasKey(p => p.Id);
            modelBuilder.Entity<ArtObject>()
                .HasOne(p => p.user)
                .WithMany(p => p.Objects)
                .HasForeignKey(p => p.UserId);


            modelBuilder.Entity<ShoppingCartItem>()
               .HasKey(p => p.Id);
            modelBuilder.Entity<ShoppingCartItem>()
                .Property(f => f.Id)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<ShoppingCartItem>()
                .HasOne(p => p.artObject)
                .WithMany(p => p.Carts)
                .HasForeignKey(p => p.artObjectId);
            modelBuilder.Entity<ShoppingCartItem>()
                .HasOne(p => p.user)
                .WithMany(p => p.shoppingCarts)
                .HasForeignKey(p => p.UserId)
                .OnDelete(DeleteBehavior.Cascade);
                ;

            modelBuilder.SeedData();
        }
    }
}
