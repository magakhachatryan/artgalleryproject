﻿using ArtGalleryDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ArtGalleryDAL.Entities
{
    public class ShoppingCartItem : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int artObjectId { get; set; }
        public int UserId { get; set; }

        public User user { get; set; }
        public ArtObject artObject { get; set; }
    }
}
