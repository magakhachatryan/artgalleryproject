﻿using ArtGalleryDAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ArtGalleryDAL.Entities
{
    public class User : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Account { get; set; }
        protected string LogIn { get; set; }
        protected string Password { get; set; }

        public ICollection<ArtObject> Objects { get; set; }

        public ICollection<ShoppingCartItem> shoppingCarts { get; set; }

    }
}

